#!/bin/bash

set -e
set -o pipefail

source "$(dirname "$0")/common.sh"

run java -jar /app/plantuml.jar -verbose -recurse **/*.puml
