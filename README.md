# Bitbucket Pipelines Pipe: plantuml-pipe

[PlantUML](https://plantuml.com) is a tool that allows to create diagrams (UML and others). They are described as code, and then processed to generate images.

This repository contains a Bitbucket pipe that simplifies processing PlantUML diagrams from [Bitbucket Pipelines](https://bitbucket.org/blog/an-introduction-to-bitbucket-pipelines).

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
    - step:
        name: Generate diagrams
        script:
          - pipe: berrueta/plantuml-pipe:master
```

By default, the pipe will find all the files with extension `.puml` in the repository and convert them to PNG images.

## Variables

None yet.

## Details

None yet.

## Prerequisites

None.

## Examples

```yaml
    - step:
        name: Generate diagrams
        script:
          - pipe: berrueta/plantuml-pipe:master
```

## Support

Diego Berrueta <diego@berrueta.net>

## Build locally

```
docker build -t berrueta/plantuml-pipe .
```

## Run locally

```
docker run -v $(pwd):$(pwd) -w $(pwd) berrueta/plantuml-pipe
```
