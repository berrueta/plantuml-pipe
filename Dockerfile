FROM miy4/plantuml
MAINTAINER berrueta <diego@berrueta.net>

COPY pipe.sh pipe.yml /

RUN wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh

ENTRYPOINT ["/pipe.sh"]
